import SignatureForm from '@/components/SignatureForm.vue'
import ShopCatalog from '@/components/ShopCatalog.vue'
import DokuForm from '@/components/DokuForm.vue'

export const routes = [
    {
        path: '',
        name: 'shop',
        component: ShopCatalog
    },
    {
        path: '/form',
        name: 'form',
        component: SignatureForm
    },
    {
        path: '/doku',
        name: 'doku',
        component: DokuForm
    }
]